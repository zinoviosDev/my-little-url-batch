package fr.marcdev.urlsbatch.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

// import java.nio.file.Path;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;

import lombok.extern.slf4j.Slf4j;

@ActiveProfiles("test")
@SpringBootTest(properties = { "command.line.runner.enabled=false" })
@Slf4j
class BatchProcessServiceTest {

	@Value("${key.parameter}") String keyParam;
	
	@Autowired
    private ApplicationContext context;
	
	@SpyBean
	private BatchProcessService batchProcessService;
	
	@BeforeEach
    void beforeExecutingTests_context_and_batchProcessService_should_be_loaded() {
        assertThat(this.context.getBean(BatchProcessService.class), is(notNullValue()));
    }
	
	@AfterAll
	static void whenAllTestsEnded() {
		log.info("All tests are finished");
	}
}
